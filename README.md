# Détection d'objet par caméra (en temps réel)

## Description

Démonstration de performance de TensorFlow lors de la présentation
concernant l'intelligence artificiel du 16/05/2018.

## Technologies utilisées

-   Langage: Python3
-   Bibliothèques : TensorFlow, OpenCV

## Comment exécuter la démonstration

1.  Installez Python 3, Cuda 9.0, CuDNN 7.0
2.  Installez VirtualEnv :
    -   `pip install virtualenv`
3.  Mettez ce projet sous un environnement virtuel :
    -   `virtualenv Object_Detect_Demo`
4.  Activez l'environnement
    -   Sous Windows : `./Scripts/activate`
    -   Sous Linux / OSx : `source Scripts/activate`
5.  Installez les paquets requis :
    -   `pip install requirements.txt`
6.  Ajoutez dans la variable d'environnement "PYTHONPATH"
    (si elle n'existe pas, créez-la):
    -   "Path_to_project/OBJECT_DETECT_DEMO/tensorflow_models"
    -   "Path_to_project/OBJECT_DETECT_DEMO/tensorflow_models/slim"
7.  Exécutez la démonstration :
    -   `OBJECT_DETECT_DEMO/object_detect_video.py`

## Module TensorFlow utilisé
[Object Detection Model](https://github.com/tensorflow/models/blob/master/research/object_detection)
