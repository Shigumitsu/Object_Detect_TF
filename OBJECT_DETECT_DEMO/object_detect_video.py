from queue import Queue
from threading import Thread

import os
import cv2
import tensorflow as tf
import numpy as np

from utils.app_utils import draw_boxes_and_labels
from object_detection.utils import label_map_util

class Camera:
    """Camera with a object detector (thxs to TF)"""
    # Path of the model
    MODEL_NAME = 'ssd_mobilenet_v2'
    PATH_TO_CKPT = os.path.join(MODEL_NAME, 'frozen_inference_graph.pb')

    # List of the strings that is used to add correct label for each box.
    PATH_TO_LABELS = 'tensorflow_models/object_detection/data/mscoco_label_map.pbtxt'
    NUM_CLASSES = 90

    # Loading label map
    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map,
                                                                max_num_classes=NUM_CLASSES,
                                                                use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    def __init__(self, url):
        # super(camera, self).__init__()
        self.__open_camera(url)
        self.__loop_camera()
        self.__close_camera()

    def __open_camera(self, url):
        self.__cap = cv2.VideoCapture(url)

    def __loop_camera(self):
        input_q = Queue(1)
        output_q = Queue()
        for _ in range(1):
            t = Thread(target=self.__worker, args=(input_q, output_q))
            t.daemon = True
            t.start()

        _, self.__frame = self.__cap.read()
        width = self.__cap.get(3)
        height = self.__cap.get(4)

        while True:
            _, self.__frame = self.__cap.read()
            input_q.put(self.__frame)

            if output_q.empty():
                pass  # fill up queue
            else:
                font = cv2.FONT_HERSHEY_SIMPLEX
                data = output_q.get()
                rec_points = data['rect_points']
                class_names = data['class_names']
                class_colors = data['class_colors']
                for point, name, color in zip(rec_points, class_names, class_colors):
                    cv2.rectangle(self.__frame,
                                  (int(point['xmin'] * width),
                                   int(point['ymin'] * height)),
                                  (int(point['xmax'] * width),
                                   int(point['ymax'] * height)),
                                  color, 3)
                    cv2.rectangle(self.__frame,
                                  (int(point['xmin'] * width),
                                   int(point['ymin'] * height)),
                                  (int(point['xmin'] * width) + len(name[0]) * 6,
                                   int(point['ymin'] * height) - 10), color, -1, cv2.LINE_AA)
                    cv2.putText(self.__frame,
                                name[0],
                                (int(point['xmin'] * width),
                                 int(point['ymin'] * height)),
                                font, 0.3, (0, 0, 0), 1)

            cv2.imshow("Object Detection", self.__frame)

            key = cv2.waitKey(1) & 0xFF
            if key == ord("q"):
                break

    def __close_camera(self):
        self.__cap.release()
        cv2.destroyAllWindows()

    def __tf_detect_objects(self, image_np, sess, detection_graph):
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

        # Each box represents a part of the image where a particular object was detected.
        boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        scores = detection_graph.get_tensor_by_name('detection_scores:0')
        classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')

        # Actual detection.
        (boxes, scores, classes, num_detections) = sess.run(
            [boxes, scores, classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})

        # Visualization of the results of a detection.
        rect_points, class_names, class_colors = draw_boxes_and_labels(
            boxes=np.squeeze(boxes),
            classes=np.squeeze(classes).astype(np.int32),
            scores=np.squeeze(scores),
            category_index=self.category_index,
            min_score_thresh=.6
        )
        return dict(rect_points=rect_points,
                    class_names=class_names,
                    class_colors=class_colors)

    def __worker(self, input_q, output_q):
        # Load a (frozen) Tensorflow model into memory.
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

            sess = tf.Session(graph=detection_graph)

        while True:
            frame = input_q.get()
            frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            output_q.put(self.__tf_detect_objects(
                frame_rgb, sess, detection_graph))

        sess.close()

Camera(0)
